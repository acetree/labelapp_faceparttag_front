/**
 * Created by Shu Zhang on 12/22/14
 * Contact: shuzhang404@gmail.com
 */


var hostUrl = 'http://54.64.157.223:5000';
var isStart = 0;
var isEnd = 0;
var lines = [];
var parts = [
    {'cnName': '左眉', 'url': 'task1-left-eyebrow.jpg', 'strokeNum':1},
    {'cnName': '右眉', 'url': 'task2-right-eyebrow.jpg', 'strokeNum':1},
    {'cnName': '左眼', 'url': 'task3-left-eye.jpg', 'strokeNum':2},
    {'cnName': '右眼', 'url': 'task4-right-eye.jpg', 'strokeNum':2},
    {'cnName': '左脸颊', 'url': 'task5-left-chin.jpg', 'strokeNum':1},
    {'cnName': '右脸颊', 'url': 'task6-right-chin.jpg', 'strokeNum':1},
    {'cnName': '嘴唇', 'url': 'task7-lip.jpg', 'strokeNum':4},
    {'cnName': '鼻子', 'url': 'task8-nose.jpg', 'strokeNum':2},
    {'cnName': '全脸', 'url': 'task9-face-mask.jpg', 'strokeNum':1}
];

var taskId = 3;
var currentLineIndex = 0;



function shiftToLogOut(){
    $('#current-task span').text('0');q
    $('#current-user span').text('未登录');
    $('#current-user a').text('登录');
}

function validateLogInInputs() {
    $.validity.start();
    $('input[name=logname]').require("请填写您的用户名");
//    $('input[name=logpwd]').require("请填写您的密码");
    var result = $.validity.end();
    return result.valid;
}


$(document).ready(function(){

    var param = document.URL;
    taskId = param.split('=')[1];

    var lineTip;
    var canvas;
    var ctx;


    function initCanvas(){
        lineTip = $("#container").appendLine({width:"2px",type:"solid",color:"yellow",beginX:0,beginY:0,endX:1,endY:1});
        canvas = document.getElementById('canvas');
        ctx = canvas.getContext('2d');
    }

    function fakeLineInput(e)
    {
        var offset = $("#canvas").offset();
        endX= e.pageX-offset.left;
        endY  = e.pageY-offset.top;
        if(isStart)
        {
            lineTip.adjustLine({
                beginX:x,
                beginY:y,
                endX:endX,
                endY:endY,
                parent:$("#canvas")
            })
        }
    }

    function drawline(){
        ctx.beginPath();
        var offset = $("#canvas").offset();
        ctx.moveTo(x,y);
        ctx.lineTo(endX,endY);
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#FF0';
        ctx.stroke();
        lineTip.hide();
    }

    function beginLine(e){

        var offset = $("#canvas").offset();
        x = e.pageX-offset.left;
        y = e.pageY-offset.top;
        console.log("begin:"+x+" "+y);
        lines[currentLineIndex].push({'x':x, 'y':y});
        lineTip.show();
    }

    function initTask(){
        lines = [];
        $('#part-cn-name').text(parts[taskId]['cnName']);
        $('#standard-image').attr('src','../static/image/task'+(parseInt(taskId)+parseInt(1))+'-0.jpg');
        $('#total-lines').text(parts[taskId]['strokeNum']);
        $('#left-lines').text(parts[taskId]['strokeNum']);
        initCanvas();
        $('input[name="cover"]').val('0');
    }

    initTask();

    $("#container").mousemove(function(e){
        fakeLineInput(e);
    });




    $("#container").click(function(e){
        if(currentLineIndex == parts[taskId]['strokeNum']){
            alert(parts[taskId]['cnName']+'最多可以画'+parts[taskId]['strokeNum']+'条折线。若您已经画完，请点击“下一个”。若需要修改，请点击“重画”。');
        }else {
            if (!isEnd) {
                if (isStart) {
                    drawline();
                    beginLine(e);

                } else {

                    isStart = 1;
                    lines.push([]);
                    beginLine(e);
                }
            } else {

                isEnd = 0;
                isStart = 1;
                lines.push([]);
                beginLine(e);
            }
        }
    });


    $("#container").contextmenu(function(){
        if(isStart) {
            currentLineIndex++;
            $('#left-lines').text(parts[taskId]['strokeNum']-currentLineIndex);
            if(currentLineIndex < parts[taskId]['strokeNum']) {
                //next line
                clearCanvas();
                lines.forEach(function(e,i,a){
                    for(var i = 0; i < e.length -1; i++){
                        ctx.beginPath();
                        ctx.moveTo(e[i]['x'],e[i]['y']);
                        ctx.lineTo(e[i+1]['x'],e[i+1]['y']);
                        ctx.lineWidth = 2;
                        ctx.strokeStyle = '#00F';
                        ctx.stroke();
                    }
                });
                $('#standard-image').attr('src', '../static/image/task' +(parseInt(taskId)+parseInt(1))+ '-' + currentLineIndex + '.jpg');
            }
            isStart = 0;
            isEnd = 1;
            drawline();

        }
        return false;
    });

    function clearCanvas(){
        $('#container').html('<canvas id="canvas" width="1024" height="736"></canvas>');
        initCanvas();
        isStart = 0;
        isEnd = 0;
    }

    $('#re-face-button').click(function(){
        lines = [];
        currentLineIndex = 0;
        clearCanvas();
    });

});